#ifndef _TINY_OAL_H
#define _TINY_OAL_H

#define _WIN32_LEAN_AND_MEAN
#include <windows.h>

#ifdef __cplusplus 
extern "C" { 
#endif 

VOID* OALPAtoVA(UINT32 pa, BOOL cached);

#ifdef __cplusplus 
} 
#endif 

#endif
