#ifndef _TINY_CRT_H
#define _TINY_CRT_H

#ifdef __cplusplus 
extern "C" { 
#endif 

void *  __cdecl Wmemset(void * _Dst, int _Val, unsigned int _Size);
void * __cdecl optmemcpy( void * _Dst, const void * _Src, unsigned int _Size);
int uint2hex(unsigned int value, int mindigits, char* hex);
int printhex(unsigned int value, int mindigits);

#ifdef __cplusplus 
} 
#endif 




#endif
