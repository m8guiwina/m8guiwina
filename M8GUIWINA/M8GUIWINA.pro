# -------------------------------------------------
# Project created by QtCreator 2009-10-13T14:01:58
# -------------------------------------------------
TARGET = M8GUIWINA
TEMPLATE = app
CONFIG(debug, debug|release) { 
    TARGET = MPack_d
    DESTDIR = ../Target/GCC/Debug
    MOC_DIR += ./tmp/moc/Debug
    OBJECTS_DIR += ./tmp/obj/Debug
}
else { 
    TARGET = MPack
    DESTDIR = ../Target/GCC/Release
    MOC_DIR += ./tmp/moc/Release
    OBJECTS_DIR += ./tmp/obj/Release
}
DEPENDPATH += .
UI_DIR += ./tmp
RCC_DIR += ./tmp
INCLUDEPATH += ./tmp \
    $MOC_DIR \
    .
SOURCES += main.cpp \
    MainUnt.cpp \
    HelloScreen.cpp \
    FontMakerUnt.cpp
HEADERS += MainUnt.h \
    HelloScreen.h \
    FontMakerUnt.h
FORMS += MainFrm.ui \
    FontMakerFrm.ui
