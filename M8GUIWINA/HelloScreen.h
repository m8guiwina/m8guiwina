#ifndef _HELLO_SCREEN_H
#define _HELLO_SCREEN_H

#define RGB565(r, g, b) ((r >> 3) << 11)| ((g >> 2) << 5)| ((b >> 3) << 0) 

void pattern();
void fillscreen(unsigned short color);
void skyblue();
void box(int x, int y, int w, int h, bool fill = false, bool fillfg = true);
void dualbox(int x, int y, int w, int h, bool fl);
void writetext(int x, int y, char* str, unsigned short fgcolor = 0xFFFF);
#ifdef GRAYFONT
void FillRect(int x, int y, int w, int h, unsigned short color = 0xFFFF);
void FrameRect(int x, int y, int w, int h, unsigned short color = 0xFFFF, int border = 1, unsigned char cornerskip = 0);
void RoundedRect(int x, int y, int w, int h, unsigned short color = 0xFFFF);
void WriteGrayText(int x, int y, char* str, unsigned short fgcolor);
void DrawExGraphic(int x, int y, int index, unsigned short fgcolor, bool hflip, bool vflip);
#endif

extern unsigned int FrameBufferAddr;

#endif

