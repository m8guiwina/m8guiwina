#include "FontMakerUnt.h"
#include "ui_FontMakerFrm.h"
#include "DbCentre.h"
#include <QtGui/QFileDialog>
#include <QtGui/QPainter>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QSet>
//#include <cmath>

//using std::log;

FontMakerFrm::FontMakerFrm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FontMakerFrm)
{
    ui->setupUi(this);

    onStripeParaChanged();

    fBaseLine = 19;
    fStripeDataFilename.clear();
    //fStripeImage
}

FontMakerFrm::~FontMakerFrm()
{
    delete ui;

    if (fStripeDataFilename.isEmpty() == false) {
        SaveStripeDataToFile(fStripeDataFilename);
    }
    SaveAppSettings();
}

void FontMakerFrm::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FontMakerFrm::closeEvent(QCloseEvent *e)
{
    e->accept();
    this->deleteLater();
}

void FontMakerFrm::onReadStripeClicked()
{
    QString imagefilename = QFileDialog::getOpenFileName(this, tr("Open Image File"), PathSetting.LastSourceFolder, "Image Files (*.png);;All Files (*.*)", 0, 0);
    if (imagefilename.isEmpty()) {
        return;
    }
    PathSetting.LastSourceFolder = QFileInfo(imagefilename).path();
    fStripeImage.load(imagefilename);
    fStripeDataFilename = imagefilename + ".stripe";
    if (QFileInfo(fStripeDataFilename).exists()) {
        LoadStripeDataFromFile(fStripeDataFilename);
    }
}

void FontMakerFrm::onPrevGraphicClicked()
{
    fCurrentGraphicIndex = NormalizeGraphicIndex(fCurrentGraphicIndex - 1);
    fGoChar = fStartChar + fCurrentGraphicIndex;
    ui->goCharEdt->setText(QChar(fGoChar));
    ShowCharOnStage();
}

void FontMakerFrm::onNextGraphicClicked()
{
    fCurrentGraphicIndex = NormalizeGraphicIndex(fCurrentGraphicIndex + 1);
    fGoChar = fStartChar + fCurrentGraphicIndex;
    ui->goCharEdt->setText(QChar(fGoChar));
    ShowCharOnStage();
}

void FontMakerFrm::onGoGraphicClicked()
{
    onStripeParaChanged();
    fCurrentGraphicIndex = fGoChar - fStartChar;
    ShowCharOnStage();
}

void FontMakerFrm::onReloadGraphicClicked()
{

}

void FontMakerFrm::onGraphicParaChanged()
{
    if (fGraphics.contains(fCurrentGraphicIndex) == false) {
        return;
    }
    TGraphicRecItem& rec = fGraphics[fCurrentGraphicIndex];
    rec.Width   = ui->widthEdt->text().toInt();
    rec.Height  = ui->heightEdt->text().toInt();
    int basedelta = ui->baseFixEdt->text().toInt() - rec.BaseFix;
    rec.BaseFix += basedelta;
    if (basedelta != 0) {
        rec.Height -= basedelta;
    }
    rec.StripeDivide = ui->stripeDiviceEdt->text().toInt();
    rec.LeftFix = ui->leftEdt->text().toInt();
    rec.RightFix = ui->rightEdt->text().toInt();
    ShowCharOnStage();
}

void FontMakerFrm::onDeleteGraphicClicked()
{
    fGraphics.remove(fCurrentGraphicIndex);
    ShowCharOnStage();
}

void FontMakerFrm::onStripeParaChanged()
{
    fStartChar = ui->startCharEdt->text().trimmed()[0].toLatin1();
    fGoChar = ui->goCharEdt->text().trimmed()[0].toLatin1();
    fGraphicCount = ui->graphicCountEdt->text().toInt();
}

void FontMakerFrm::onSetStripeParaClicked()
{

}

bool FontMakerFrm::ShowCharOnStage()
{
    bool Result = false;
    if (fCurrentGraphicIndex < 0) {
        return Result;
    }
    if (fGraphics.contains(fCurrentGraphicIndex) == false) {
        // TODO: make up a new one
        TGraphicRecItem item;
        if (fGraphics.contains(NormalizeGraphicIndex(fCurrentGraphicIndex - 1))) {
            // go next. follow previous
            TGraphicRecItem prev = fGraphics[NormalizeGraphicIndex(fCurrentGraphicIndex - 1)];
            item = prev;
            //item.StripeDivide += prev.Width + 2;
            item.StripeDivide += prev.Width; // we assume previous graphic is well done
            DeterminateGraphic(item);

        } else if (fGraphics.contains(NormalizeGraphicIndex(fCurrentGraphicIndex + 1))) {
            // go previous. take argument from next graphic
            TGraphicRecItem next = fGraphics[NormalizeGraphicIndex(fCurrentGraphicIndex + 1)];
            item = next;
            //item.StripeDivide -= next.Width + 2;
            item.StripeDivide -= next.Width;
            DeterminateGraphic(item);
        } else {
            item.StripeDivide = fCurrentGraphicIndex * 13;
            item.Height = fStripeImage.height();
        }
        fGraphics.insert(fCurrentGraphicIndex, item);
    }
    TGraphicRecItem rec = fGraphics[fCurrentGraphicIndex];
    RefreshGraphicPara();

    QImage qimage = QImage(ui->zoomView->size(), QImage::Format_ARGB32);
    qimage.fill(0xFFF0D8C4);
    QPainter painter(&qimage);
    QMatrix matrix;
    matrix.scale(18, 18);
    painter.setMatrix(matrix); // copy
    painter.drawImage(1, 1, fStripeImage, rec.StripeDivide - 4, 0, rec.Width + 8, fStripeImage.height());
    painter.resetMatrix();
    QImage texture = QImage(9, 9, QImage::Format_ARGB32);
    texture.fill(0x00FFFFFF); // alpha
    QPainter tpainter(&texture);
    tpainter.setPen(QPen(QColor(0xFFE0E0E0)));
    tpainter.drawLine(8,0,8,8);
    tpainter.drawLine(8,8,0,8);
    tpainter.end();
    QBrush brush;
    brush.setTextureImage(texture);
    matrix.reset();
    matrix.scale(2, 2);
    painter.setMatrix(matrix); // copy
    painter.fillRect(qimage.rect(), brush);

    matrix.translate(0.5, 0.5); // workaround
    painter.setMatrix(matrix); // copy
    painter.setPen(QPen(QColor(0xC0D5AE35), 2)); // yellow
    painter.drawLine(0, (fBaseLine + 1) * 9 - 1, qimage.width() - 1, (fBaseLine + 1) * 9 - 1); // baseline

    painter.setPen(QPen(QColor(0xA029961B), 2)); // green
    painter.drawLine(5 * 9 - 1, 0, 5 * 9 - 1, (fStripeImage.height() + 2) * 9);
    if (rec.Width != 0) {
        painter.drawLine((rec.Width + 5) * 9 - 1, 0, (rec.Width + 5) * 9 - 1, (fStripeImage.height() + 2) * 9);
    }
    if (rec.Height != 0) {
        painter.drawLine(0, (fBaseLine - (rec.Height + rec.BaseFix) + 1) * 9 - 1, (rec.Width + 10) * 9 - 1, (fBaseLine - (rec.Height + rec.BaseFix) + 1) * 9 - 1);
    }

    painter.setPen(QPen(QColor(0x60D553CD), 2)); // pink
    if (rec.LeftFix != 0) {
        painter.drawLine((rec.LeftFix + 5) * 9 - 1, 0, (rec.LeftFix + 5) * 9 - 1, (fStripeImage.height()  + 2) * 9);
    }
    if (rec.RightFix != 0) {
        painter.drawLine((rec.Width - rec.RightFix + 5) * 9 - 1, 0, (rec.Width - rec.RightFix + 5) * 9 - 1, (fStripeImage.height()  + 2) * 9);
    }
    if (rec.BaseFix != 0) {
        painter.drawLine(0, (fBaseLine - rec.BaseFix + 1) * 9 - 1, (rec.Width + 10) * 9 - 1, (fBaseLine - rec.BaseFix + 1) * 9 - 1);
    }

    ui->zoomView->setPixmap(QPixmap::fromImage(qimage));

    return Result;
}


void FontMakerFrm::DeterminateGraphic( TGraphicRecItem &item )
{
    for (int x = item.StripeDivide; x < item.StripeDivide + 6; x++) {
        bool allwhite = true;
        for (int y = 0; y < fStripeImage.height(); y++) {
            if (fStripeImage.pixel(x, y) != 0xFFFFFFFF) {
                allwhite = false;
                break;
            }
        }
        if (allwhite == false) {
            item.StripeDivide = x;
            break;
        }
    }
    // else = 4?
    for (int x = item.StripeDivide; x < item.StripeDivide + 25; x++) {
        bool allwhite = true;
        for (int y = 0; y < fStripeImage.height(); y++) {
            if (fStripeImage.pixel(x, y) != 0xFFFFFFFF) {
                allwhite = false;
                break;
            }
        }
        if (allwhite == true) {
            item.Width = x - item.StripeDivide;
            break;
        }
    }
    // baseline
    for (int y = fStripeImage.height() - 1; y > 0; y--) {
        bool allwhite = true;
        for (int x = item.StripeDivide; x < item.StripeDivide + item.Width; x++) {
            if (fStripeImage.pixel(x, y) != 0xFFFFFFFF) {
                allwhite = false;
                break;
            }
        }
        if (allwhite == false) {
            item.BaseFix = fBaseLine - (y + 1);
            break;
        }
    }
    for (int y = 0; y < fStripeImage.height(); y++) {
        bool allwhite = true;
        for (int x = item.StripeDivide; x < item.StripeDivide + item.Width; x++) {
            if (fStripeImage.pixel(x, y) != 0xFFFFFFFF) {
                allwhite = false;
                break;
            }
        }
        if (allwhite == false) {
            item.Height = fBaseLine - y - item.BaseFix;
            break;
        }
    }
}

bool FontMakerFrm::RefreshGraphicPara()
{
    if (fGraphics.contains(fCurrentGraphicIndex) == false) {
        return false;
    }
    TGraphicRecItem rec = fGraphics[fCurrentGraphicIndex];
    ui->widthEdt->setText(QString("%1").arg(rec.Width, 1, 10));
    ui->heightEdt->setText(QString("%1").arg(rec.Height, 1, 10));
    ui->leftEdt->setText(QString("%1").arg(rec.LeftFix, 1, 10));
    ui->rightEdt->setText(QString("%1").arg(rec.RightFix, 1, 10));
    ui->stripeDiviceEdt->setText(QString("%1").arg(rec.StripeDivide, 1, 10));
    ui->baseFixEdt->setText(QString("%1").arg(rec.BaseFix, 1, 10));
    return true;
}


void FontMakerFrm::RefreshStripePara()
{
    ui->startCharEdt->setText(QChar(fStartChar));
    ui->goCharEdt->setText(QChar(fGoChar));
    ui->graphicCountEdt->setText(QString("%1").arg(fGraphicCount, 1, 10));
}

bool FontMakerFrm::SaveStripeDataToFile( QString filename )
{
    bool Result = false;

    QFile file(filename);
    file.open(QFile::WriteOnly);
    // TODO: UCL/BZ2 Compression
    QDataStream writer(&file);
    writer << fGraphicCount << fBaseLine << fStartChar << fGoChar << fCurrentGraphicIndex;
    writer << fGraphics;
    file.close();
    Result = true;

    return Result;
}

bool FontMakerFrm::LoadStripeDataFromFile( QString filename )
{
    bool Result = false;

    QFile file(filename);
    file.open(QFile::ReadOnly);
    // TODO: MIME check
    QDataStream reader(&file);
    reader >> fGraphicCount >> fBaseLine >> fStartChar >> fGoChar >> fCurrentGraphicIndex;
    reader >> fGraphics;
    file.close();
    Result = true;

    RefreshStripePara();
    ShowCharOnStage();

    return Result;
}

void FontMakerFrm::onExportFontClicked()
{
    QString fontfilename = QFileDialog::getSaveFileName(this, tr("Save Font Source"), PathSetting.LastFontFileFolder + "/Candara.c", "Source Files (*.c);;All Files (*.*)", 0, 0);
    if (fontfilename.isEmpty()) {
        return;
    }
    PathSetting.LastFontFileFolder = QFileInfo(fontfilename).path();
    ExportFontDataAsSource(fontfilename);
}

int ilog2(int i) {
    // 17 -> 5, 16 -> 4
    int Result = 0;
    i = abs(i) - 1;
    while (i > 0) {
        i = i >> 1;
        Result++;
    }
    return Result;
}

bool FontMakerFrm::ExportFontDataAsSource( QString filename )
{
    bool Result = false;

    QString headername;
    if (filename.endsWith(".c", Qt::CaseInsensitive)) {
        headername = filename.left(filename.lastIndexOf(".c", -1, Qt::CaseInsensitive)) + ".h";
    } else {
        headername = filename  +".h";
    }

    QFile file(filename);
    QFile headerfile(headername);
    file.open(QFile::WriteOnly);
    headerfile.open(QFile::WriteOnly);
    QTextStream writer(&file);
    QTextStream headerwriter(&headerfile);
    QString stripemark = QString("// This file is generate by FontMaker. Design data: %1 \r\n\r\n").arg(fStripeDataFilename);
    QString headermark = QFileInfo(headername).fileName();
    if (headermark.contains('.')) {
        headermark = headermark.left(headermark.lastIndexOf('.')).toUpper();
    }
    headerwriter << QString("#ifndef %1_H\r\n").arg(headermark);
    headerwriter << QString("#define %1_H\r\n\r\n").arg(headermark);
    headerwriter << stripemark;
    writer << stripemark;
    headerwriter << QString("#ifdef __cplusplus\r\nextern \"C\" { \r\n#endif\r\n\r\n");
    headerwriter << "unsigned char GraphicData[];\r\n";
    writer << "unsigned char GraphicData[] = {\r\n    ";
    int sum = 0;

    QMap < int, int > WidthTable, HeightTable, LeftFix, RightFix, BaseFix, FastTable;
    foreach(int index, fGraphics.keys()) {
        if (index >= fGraphicCount) {
            // cleanup?
            continue;
        }
        TGraphicRecItem rec = fGraphics[index];
        writer << QString("// Char %1 (%2*%3) \r\n    ")
            .arg(fStartChar + index > 32 && fStartChar + index < 127?QChar(fStartChar + index):QString("0x%1").arg(fStartChar + index, 2, 16, QLatin1Char('0')))
            .arg(rec.Width).arg(rec.Height);
        WidthTable.insertMulti(rec.Width, index);
        HeightTable.insertMulti(rec.Height, index);
        LeftFix.insertMulti(rec.LeftFix, index);
        RightFix.insertMulti(rec.RightFix, index);
        BaseFix.insertMulti(rec.BaseFix, index);
        QByteArray combinebits((rec.Width * rec.Height + 1) / 2, char(0));
        int pos = 0, dpos = 0;
        for (int x = rec.StripeDivide; x < rec.Width + rec.StripeDivide; x++) {
            for (int y = fBaseLine - rec.BaseFix - rec.Height; y < fBaseLine - rec.BaseFix; y++) {
                if (pos % 2 == 0) {
                    combinebits[dpos] = fStripeImage.pixelIndex(x, y); // 0~15
                } else {
                    combinebits[dpos] = combinebits[dpos] | (fStripeImage.pixelIndex(x, y) << 4);
                    dpos++;
                }
                pos++;
            }            
        }
        pos = 0;
        foreach(unsigned char b, combinebits) {
            writer << "0x" << QString("%1, ").arg(b, 2, 16, QLatin1Char('0')).toUpper();
            pos++;
            if (pos == 16) {
                writer << "\r\n    ";
                pos = 0;
            }
        }
        if (pos != 0) {
            writer << "\r\n    ";
        }
        FastTable.insert(index, sum);
        sum += combinebits.size();
    }
    writer << QString("// %1 of %2 Graphics, %3 Bytes.\r\n").arg(fGraphics.size()).arg(fGraphicCount).arg(sum);
    writer << "};\r\n\r\n";

    headerwriter << "unsigned short FastTable[];\r\n";
    writer << "unsigned short FastTable[] = {\r\n    ";
    for (int i = 0; i < fGraphicCount; i++) {
        if (FastTable.contains(i)) {
            writer << QString("%1, ").arg(FastTable[i]);
        } else {
            writer << "65535, ";
        }
        if (i % 16 == 15 && i > 0 && i < fGraphicCount - 1) {
            writer << "\r\n    ";
        }
    }
    writer << "\r\n};\r\n\r\n";

    QList < int > WidthKeys     = WidthTable.uniqueKeys(); // sorted
    QList < int > HeightKeys    = HeightTable.uniqueKeys();
    QList < int > LeftKeys      = LeftFix.uniqueKeys();
    QList < int > RightKeys     = RightFix.uniqueKeys();
    QList < int > BaseKeys      = BaseFix.uniqueKeys();

    int widthbits = ilog2(WidthKeys.size()); // 1 -> 0, 2 -> 1, 3,4 -> 2
    if (widthbits > 0) {
        headerwriter << "unsigned char WidthTable[];\r\n";
        writer << "unsigned char WidthTable[] = {\r\n    ";
        writer << QString("// %1 bits \r\n    ").arg(widthbits);
        foreach(int w, WidthKeys) {
            writer << QString("%1, ").arg(w);
        }
        //writer.flush();
        //writer.seek(writer.pos() - 2);
        writer << "\r\n    // ";
        foreach(int w, WidthKeys) {
            writer << QString("%1 (%2), ").arg(w).arg(WidthTable.values(w).size());
        }
        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define WIDTHTABLEVALUE %1\r\n").arg(WidthTable.uniqueKeys().at(0));
    }

    int heightbits = ilog2(HeightKeys.size());
    if (heightbits > 0) {
        headerwriter << "unsigned char HeightTable[];\r\n";
        writer << "unsigned char HeightTable[] = {\r\n    ";
        writer << QString("// %1 bits \r\n    ").arg(heightbits);
        foreach(int h, HeightKeys) {
            writer << QString("%1, ").arg(h);
        }
        writer << "\r\n    // ";
        foreach(int h, HeightKeys) {
            writer << QString("%1 (%2), ").arg(h).arg(HeightTable.values(h).size());
        }
        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define HEIGHTTABLEVALUE %1\r\n").arg(HeightTable.uniqueKeys().at(0));
    }

    int leftbits = ilog2(LeftKeys.size());
    if (leftbits > 0) {
        if (heightbits == 0) {
            writer << "\r\n";
        }
        headerwriter << "unsigned char LeftFix[];\r\n";
        writer << "unsigned char LeftFix[] = {\r\n    ";
        writer << QString("// %1 bits \r\n    ").arg(leftbits);
        foreach(int l, LeftKeys) {
            writer << QString("%1, ").arg(l);
        }
        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define LEFTFIXVALUE %1\r\n").arg(LeftFix.uniqueKeys().at(0));
    }

    int rightbits = ilog2(RightKeys.size());
    if (rightbits > 0) {
        if (leftbits == 0) {
            writer << "\r\n";
        }
        headerwriter << "unsigned char RightFix[];\r\n";
        writer << "unsigned char RightFix[] = {\r\n    ";
        writer << QString("// %1 bits \r\n    ").arg(rightbits);
        foreach(int r, RightKeys) {
            writer << QString("%1, ").arg(r);
        }
        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define RIGHTFIXVALUE %1\r\n").arg(RightFix.uniqueKeys().at(0));
    }

    int basebits = ilog2(BaseKeys.size());
    if (basebits > 0) {
        if (rightbits == 0) {
            writer << "\r\n";
        }
        headerwriter << "char BaseFix[];\r\n";
        writer << "char BaseFix[] = {\r\n    ";
        writer << QString("// %1 bits \r\n    ").arg(basebits);
        foreach(int b, BaseKeys) {
            writer << QString("%1, ").arg(b);
        }
        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define BASEFIXVALUE %1\r\n").arg(BaseFix.uniqueKeys().at(0));
    }

    headerwriter << "unsigned short ParaTable[];\r\n";
    writer << "unsigned short ParaTable[] = {\r\n    ";
    for (int i = 0; i < fGraphicCount; i++) {
        if (FastTable.contains(i)) {
            int widthindex = WidthTable.key(i); // 12, 13, 5
            widthindex = WidthKeys.indexOf(widthindex);
            int heightindex = HeightKeys.indexOf(HeightTable.key(i));
            int leftindex = LeftKeys.indexOf(LeftFix.key(i));
            int rightindex = RightKeys.indexOf(RightFix.key(i));
            int baseindex = BaseKeys.indexOf(BaseFix.key(i));
            unsigned short ParaItem = 0;
            ParaItem |= widthindex;
            ParaItem |= (heightindex << widthbits);
            ParaItem |= (leftindex << (widthbits + heightbits));
            ParaItem |= (rightindex << (widthbits + heightbits + leftbits));
            ParaItem |= (baseindex << (widthbits + heightbits + leftbits + rightbits));
            writer << QString("%1, ").arg(ParaItem);
        } else {
            writer << "65535, ";
        }
        if (i % 16 == 15 && i > 0 && i < fGraphicCount - 1) {
            writer << "\r\n    ";
        }
    }
    writer << "\r\n};\r\n\r\n";

    QVector <QRgb> GrayTable = fStripeImage.colorTable();
    if (GrayTable.size() > 1) {
        headerwriter << "unsigned char AlphaTable[];\r\n";
        writer << "unsigned char AlphaTable[] = {\r\n    ";

        foreach (QRgb c, GrayTable) {
            writer << QString("%1, ").arg(255 - qGray(c)); // 0, 255
        }

        writer << "\r\n};\r\n\r\n";
    } else {
        headerwriter << QString("#define ALPHAFIXVALUE %1\r\n").arg(BaseFix.uniqueKeys().at(0));
    }

    headerwriter << QString("#define BASELINE %1\r\n").arg(fBaseLine);
    headerwriter << QString("#define STARTCHAR '%1'\r\n").arg(QChar(fStartChar));
    headerwriter << QString("#define GRAPHICCOUNT %1\r\n").arg(fGraphicCount);
    headerwriter << QString("#define WIDTHBITS %1\r\n").arg(widthbits);
    headerwriter << QString("#define HEIGHTBITS %1\r\n").arg(heightbits);
    headerwriter << QString("#define LEFTBITS %1\r\n").arg(leftbits);
    headerwriter << QString("#define RIGHTBITS %1\r\n").arg(rightbits);
    headerwriter << QString("#define BASEBITS %1\r\n").arg(basebits);

    headerwriter << QString("\r\n#ifdef __cplusplus\r\n} \r\n#endif\r\n\r\n");
    headerwriter << QString("#endif // %1_H\r\n\r\n").arg(headermark);

    file.close();
    headerfile.close();
    Result = true;

    return Result;
}

int FontMakerFrm::NormalizeGraphicIndex( int index )
{
    int Result = index;
    if (fGraphicCount == 0) {
        return Result;
    }
    if (Result >= fGraphicCount) {
        Result = Result % fGraphicCount;
    }
    if (Result < 0) {
        Result = Result % fGraphicCount + fGraphicCount;
    }
    return Result;
}

tagGraphicRecItem::tagGraphicRecItem()
{
    StripeDivide = 0;
    LeftFix = 0;
    RightFix = 0;
    BaseFix = 0;
    Width = 15;
    Height = 25;
}

QDataStream & operator<<( QDataStream &out, const char &c )
{
    out << qint8(c);
    return out;
}

QDataStream & operator>>( QDataStream &out, char &c )
{
    out >> reinterpret_cast<qint8&>(c);
    return out;
}

QDataStream & operator<<( QDataStream &out, const TGraphicRecItem &graphic )
{
    out.writeRawData((char*)&graphic, sizeof(graphic));
    return out;
}


QDataStream & operator>>( QDataStream &in, TGraphicRecItem &graphic )
{
    in.readRawData((char*)&graphic, sizeof(graphic));
    return in;
}