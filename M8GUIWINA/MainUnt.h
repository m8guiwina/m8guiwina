#ifndef MAINUNT_H
#define MAINUNT_H

#include <QMainWindow>
#include <QImage>
#include <QThread>
#include <QLabel>

class MyThread : public QThread
{
private:
    QLabel* StageWidget;
    QImage* StageImage;
    bool Keeping;
public:
    void AssignStage(QLabel* widget, QImage* image);
public:
    void run();
    void StopKeeping();
};

namespace Ui {
    class TMainFrm;
}

class TMainFrm : public QMainWindow {
    Q_OBJECT
public:
    TMainFrm(QWidget *parent = 0);
    ~TMainFrm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::TMainFrm *ui;
private:
    QImage fStageProxy;
    MyThread fStageRepainter;
private slots:
    void onPatternClicked();
    void onClassicBootClicked();
    void onModernBootClicked();
    void onFontMakerClicked();
};



#endif // MAINUNT_H
