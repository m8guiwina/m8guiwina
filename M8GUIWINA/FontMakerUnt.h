#ifndef FONTMAKERUNT_H
#define FONTMAKERUNT_H

#include <QtGui/QDialog>
#include <QtCore/QMap>
#include <QtCore/QDataStream>
#include <QtGui/QCloseEvent>


typedef struct tagGraphicRecItem {
    int StripeDivide;
    int LeftFix;
    int RightFix;
    int BaseFix;
    int Width;
    int Height;
    tagGraphicRecItem();
} TGraphicRecItem, *PGraphicRecItem;

typedef QMap < int, TGraphicRecItem > TGraphicRecs;

QDataStream &operator<<(QDataStream &out, const char &c);
QDataStream &operator<<(QDataStream &out, const TGraphicRecItem &graphic);

QDataStream &operator>>(QDataStream &in, char &c);
QDataStream &operator>>(QDataStream &in, TGraphicRecItem &graphic);

namespace Ui {
    class FontMakerFrm;
}

class FontMakerFrm : public QDialog {
    Q_OBJECT
public:
    FontMakerFrm(QWidget *parent = 0);
    ~FontMakerFrm();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *e);

private:
    Ui::FontMakerFrm *ui;
private:
    QImage fStripeImage;
    QString fStripeDataFilename;
    int fGraphicCount, fCurrentGraphicIndex, fBaseLine;
    char fStartChar, fGoChar;
    TGraphicRecs fGraphics;

private:
    bool ShowCharOnStage();
    void DeterminateGraphic(TGraphicRecItem &item);
    bool RefreshGraphicPara();
    void RefreshStripePara();
    bool SaveStripeDataToFile(QString filename);
    bool LoadStripeDataFromFile(QString filename);
    bool ExportFontDataAsSource(QString filename);
    int NormalizeGraphicIndex(int index);
private slots:
    void onReadStripeClicked();
    void onPrevGraphicClicked();
    void onNextGraphicClicked();
    void onGoGraphicClicked();
    void onReloadGraphicClicked();
    void onDeleteGraphicClicked();
    void onGraphicParaChanged();
    void onStripeParaChanged();
    void onSetStripeParaClicked();
    void onExportFontClicked();
};

#endif // FONTMAKERUNT_H
