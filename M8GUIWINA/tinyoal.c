#include "tinyoal.h"

// M8 only
VOID* OALPAtoVA(UINT32 pa, BOOL cached) {
    // we assume we run in pa mode (supervisor)
    return (VOID*)pa;
    // TODO: check CPSR_C
    if (cached) {
        return (VOID*)(pa + 0x30000000UL);
    } else {
        return (VOID*)(pa + 0x50000000UL);
    }
}
