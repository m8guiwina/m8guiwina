#include "tinycrt.h"
#include "tinydebug.h"

void * __cdecl Wmemset (
                       void *dst,
                       int val,
                       unsigned int count
                       )
{
    void *start = dst;

    while (count--) {
        *(unsigned short *)dst = (unsigned short)val;
        dst = (unsigned short *)dst + 1;
    }
    /*unsigned short* start  = dst;
    for (; start < count + start; start++) {
        *start = (unsigned short)val;
    }*/
    return(start);
}

void * __cdecl optmemcpy( void * _Dst, const void * _Src, unsigned int _Size) {
    char *dst8 = (char *)_Dst;
    char *src8 = (char *)_Src;

    if (_Size & 1) {
        dst8[0] = src8[0];
        dst8 += 1;
        src8 += 1;
    }

    _Size /= 2;
    while (_Size--) {
        dst8[0] = src8[0];
        dst8[1] = src8[1];

        dst8 += 2;
        src8 += 2;
    }
    return _Dst;
}


unsigned char QuadBit2Hex(unsigned char num) {
    if (num < 10) {
        return num + '0';
    } else {
        return num + '7';
    }
}

// for printf
int uint2hex(unsigned int value, int mindigits, char* hex) {
    char Buf[9];
    char* Dest;

    Dest = &Buf[8];
    *Dest = 0;
    do {
        Dest--;
        *Dest = '0';
        if (value != 0) {
            *Dest = QuadBit2Hex(value & 0xF);
            value = value >> 4;
        }
        mindigits--;
    } while (value != 0 || mindigits > 0);
    // then copy it to output
    while (Dest != &Buf[8]) {
        //
        *hex = *Dest;
        hex++;
        Dest++;
    }
    return 1;
}

int printhex(unsigned int value, int mindigits) {
    char Buf[9];
    uint2hex(value, mindigits, &Buf[0]);
    OEMWriteDebugString(&Buf[0]);
    return 0;
}