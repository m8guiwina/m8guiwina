#ifndef CANDARA_H
#define CANDARA_H

// This file is generate by FontMaker. Design data: G:/root/Projects/MyPrj/M8TinyLoader/M8TinyLoader/PpsDES/FontsStripe_16c.png.stripe 

#ifdef __cplusplus
extern "C" { 
#endif

unsigned char GraphicData[];
unsigned short FastTable[];
unsigned char WidthTable[];
unsigned char HeightTable[];
#define LEFTFIXVALUE 0
#define RIGHTFIXVALUE 0
char BaseFix[];
unsigned short ParaTable[];
unsigned char AlphaTable[];
#define BASELINE 19
#define STARTCHAR '!'
#define GRAPHICCOUNT 96
#define WIDTHBITS 5
#define HEIGHTBITS 4
#define LEFTBITS 0
#define RIGHTBITS 0
#define BASEBITS 4

#ifdef __cplusplus
} 
#endif

#endif // CANDARA_H

