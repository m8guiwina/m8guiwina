#ifndef _DBCENTRE_H
#define _DBCENTRE_H

typedef struct tagPathRecItem {
    QString LastProjectFolder;
    QString LastStockXipFolder;
    QString LastFontFileFolder;
    QString LastDumpFolder;
    QString LastSourceFolder;
    int LastSelectedItemIndex;
} TPathRecItem;

typedef struct tagStateRecItem {
    bool WindowMaxium;
    QByteArray MainFrmState;
    QByteArray ProjectLayoutState;
    QByteArray MessageLayoutState;
    bool RegEditorMaxium;
    QByteArray RegFrmState;
    QByteArray KeyLayoutState;
} TStateRecItem;

extern TPathRecItem PathSetting;
extern TStateRecItem StateSetting;

void LoadAppSettings( void );
void SaveAppSettings( void );


#endif
